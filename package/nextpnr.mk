PACKAGE=nextpnr
NEXTPNR_REPO=https://github.com/YosysHQ/nextpnr
NEXTPNR_DEP=icestorm-build
NEXTPNR_SYS_DEP= libboost-dev libboost-filesystem-dev libboost-thread-dev libboost-program-options-dev libboost-iostreams-dev cmake python3 git make libeigen3-dev
define NEXTPNR_CONFIGURE
	cd $(NEXTPNR_SRC_DIR) && cmake . -DARCH=ice40 -DICESTORM_INSTALL_PREFIX=$(INSTALL_DIR) -DCMAKE_INSTALL_PREFIX=$(INSTALL_DIR)
endef

define NEXTPNR_BUILD
	make -C $(NEXTPNR_SRC_DIR)
endef

define NEXTPNR_INSTALL
	make -C $(NEXTPNR_SRC_DIR) install
endef


$(eval $(generic-package-vars))







