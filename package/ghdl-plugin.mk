PACKAGE=ghdl-plugin
GHDL_PLUGIN_REPO=https://github.com/ghdl/ghdl-yosys-plugin
GHDL_PLUGIN_DEP=ghdl-build

define GHDL_PLUGIN_CONFIGURE

endef

define GHDL_PLUGIN_BUILD
        PATH=$(PATH) make -C $(GHDL_PLUGIN_SRC_DIR) 
endef

define GHDL_PLUGIN_INSTALL
        PATH=$(PATH) make -C $(GHDL_PLUGIN_SRC_DIR) install
endef

$(eval $(generic-package-vars))



