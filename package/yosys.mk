PACKAGE=yosys
YOSYS_REPO=https://github.com/YosysHQ/yosys
YOSYS_SYS_DEP=build-essential clang bison flex libreadline-dev gawk tcl-dev libffi-dev git graphviz xdot pkg-config python3 libboost-system-dev libboost-python-dev libboost-filesystem-dev zlib1g-dev 

define YOSYS_CONFIGURE
	make -C $(YOSYS_SRC_DIR) config-gcc
endef

define YOSYS_BUILD
	make -C $(YOSYS_SRC_DIR) PREFIX=$(INSTALL_DIR)
endef

define YOSYS_INSTALL
	make -C $(YOSYS_SRC_DIR) PREFIX=$(INSTALL_DIR) install
endef

$(eval $(generic-package-vars))








