PACKAGE=icestorm
ICESTORM_REPO=https://github.com/yosyshq/icestorm
ICESTORM_SYS_DEP= libftdi-dev

define ICESTORM_CONFIGURE

endef

define ICESTORM_BUILD
	make -C $(ICESTORM_SRC_DIR) PREFIX=$(INSTALL_DIR)
endef

define ICESTORM_INSTALL
	make -C $(ICESTORM_SRC_DIR) PREFIX=$(INSTALL_DIR) install
endef



$(eval $(generic-package-vars))
