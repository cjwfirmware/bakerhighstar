PACKAGE=ghdl
GHDL_REPO=https://github.com/ghdl/ghdl
GHDL_DEP=yosys-build
GHDL_SYS_DEP= gnat

define GHDL_CONFIGURE
        cd $(GHDL_SRC_DIR) && ./configure --prefix=$(INSTALL_DIR) --enable-synth
endef

define GHDL_BUILD
        make -C $(GHDL_SRC_DIR)
endef

define GHDL_INSTALL
        make -C $(GHDL_SRC_DIR) install
endef
$(eval $(generic-package-vars))



