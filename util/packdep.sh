#!/bin/sh                                                                                                                 

res=""
quiet=""

for var in "$@"
do
    if [ "$var" = "-q" ]
    then
        quiet="quiet"
    else
        vl=`dpkg-query -W -f'${Status}\n' $var 2>/dev/null`
        if [ "$vl" != "install ok installed" ]
        then
            res="$res $var"
	fi
    fi
done

if [ -z "$res" ]
then
    exit 0
fi

if [ -z "$quiet" ]
then
    echo "CRITICAL PACKAGES MISSING"
    echo $res
    exit 1
fi

echo $res

